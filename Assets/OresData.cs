﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[System.Serializable]
public class Ore
{
    public string Name;
    [ShowAssetPreview(32, 32)]
    public Sprite Sprite;
    public int SellValue;
}

public class OresData : MonoBehaviour
{
    public static OresData Instance;
    public Ore[] Ores_Ar;

    public void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }


}
