﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

[System.Serializable]
public class Level
{
    public string Name;
    public string Description;
    public int Cost;
    public int UnlockCost;
}

public class LevelSelector : MonoBehaviour
{
    public static LevelSelector Instance;
    public List<Level> Level_List = new List<Level>();
    public TextMeshProUGUI Name_Text, Description_Text, CostText;
    public Button Play_Button;
    public int LevelSelected;

    private void Awake()
    {
        Instance = this;
    }

    public void SelectionLevel(int IdLevel)
    {
        try
        {
            Name_Text.text = Level_List[IdLevel].Name;
            Description_Text.text = Level_List[IdLevel].Description;
            CostText.text = "Cost : " + Level_List[IdLevel].Cost;
            Play_Button.interactable = GameManager.Instance.I_Money >= Level_List[IdLevel].Cost;//desac le buton si jamais il n'a pas assez d'argent
            LevelSelected = IdLevel;

        }
        catch (System.Exception)
        {
            Debug.Log("Aucun level fourni");
        }
    }

    public void StartLevel()
    {
        GameObject.Find("EventSystem").SetActive(false);
        StartCoroutine(StartLevel_Cor());
    }

    IEnumerator StartLevel_Cor()
    {
        yield return BlackScreen.Instance.screen.DOFade(1, 2f).WaitForCompletion();
        GameManager.Instance.LoadLevel(LevelSelected + 1, Level_List[LevelSelected].Cost);
    }

    

}
