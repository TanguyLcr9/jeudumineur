﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCanvasManager : MonoBehaviour
{
    public static LevelCanvasManager Instance;
    public GameObject Lvl_Left, LvlRight, Lvl_Add;
    public List<GameObject> LvlPanel_List = new List<GameObject>();

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }


    public void UpdateLevels()
    {
        bool direction = true;
        for(int i = 0; i < GameManager.Instance.Lvl + 1; i++)
        {
            
            if (i >= LevelSelector.Instance.Level_List.Count)    break;
            

            GameObject newLvl = null;
            
            if (i < LvlPanel_List.Count)
            {
                newLvl = LvlPanel_List[i];
            }else
            {
                newLvl = Instantiate(direction ? LvlRight : Lvl_Left, transform);
                LvlPanel_List.Add(newLvl);
            }

            newLvl.GetComponent<LevelPanel>().SetUpPanel(i);

            if (i != GameManager.Instance.Lvl)
                newLvl.GetComponent<LevelPanel>().UnLockLevel();
            else
            {
                newLvl.GetComponent<LevelPanel>().UnLockLevel(false);
            }

            direction = !direction;
        }
    }

   
}
