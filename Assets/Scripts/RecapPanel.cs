﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RecapPanel : MonoBehaviour
{
    
    public int[] NumbersToReach;
    public TextMeshProUGUI[] Results_Text;

    int[] numbers;
    string[] defalutTitle;

    Coroutine myCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        defalutTitle = new string[Results_Text.Length];
        for (int i = 0; i < Results_Text.Length; i++) {
            defalutTitle[i] = Results_Text[i].text;

                }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            StopCoroutine(myCoroutine);
            DiplayFinalNumbers(NumbersToReach);
        }
    }

    public void DoDisplayNumbers(int[] newNumbers)
    {
        NumbersToReach = newNumbers;
        numbers = new int[NumbersToReach.Length];
        myCoroutine = StartCoroutine(DisplayNumbers());
    }

    public void DiplayFinalNumbers(int[] newNumbers) {

        NumbersToReach = newNumbers;
        for (int i = 0; i < Results_Text.Length; i++)
        {
            Results_Text[i].text = defalutTitle[i] + " " + NumbersToReach[i];
        }
    }

    IEnumerator DisplayNumbers()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < Results_Text.Length; i++)
        {
            Results_Text[i].text = defalutTitle[i] + " 0";
        }
            for (int i = 0; i < Results_Text.Length; i++)
        {
            while(numbers[i] < NumbersToReach[i])
            {
                numbers[i]++;
                Results_Text[i].text = defalutTitle[i] + " " + numbers[i];

                yield return new WaitForSeconds(0.05f);
            }

            yield return new WaitForSeconds(1f);
        }
    }
}
