﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class LevelPanel : MonoBehaviour
{
    public GameObject LevelGO, LockGO;
    public GameObject ImageBlocked, ImageConduct;
    public TextMeshProUGUI NumberTMP, UnlockCostTMP;
    public Button playButton, lockButton;
    UnityAction MyAction;
    int levelID;

    public void SetUpPanel(int lvl)
    {
        playButton.onClick.AddListener(OpenLevelDescription);
        levelID = lvl;
        NumberTMP.text = "" + (lvl + 1);
        Conduct(levelID != GameManager.Instance.Lvl -1);//Si faux alors conduit sinon bloque le tunnel

            UnlockCostTMP.text = "Unlock?\n" + LevelSelector.Instance.Level_List[levelID].UnlockCost;
            lockButton.interactable = GameManager.Instance.I_Money >= LevelSelector.Instance.Level_List[levelID].UnlockCost;
        
    }

    void OpenLevelDescription()
    {
        MenuCanvas.Instance.OpenLevelDescription(levelID);
    }

    public void Conduct(bool state = true)//fais conduire le tunel
    {
        ImageBlocked.SetActive(!state);
        ImageConduct.SetActive(state);
    }

    public void UnLockLevel(bool state = true)
    {
        LockGO.SetActive(!state);
        LevelGO.SetActive(state);
    }

    public void Pay()
    {
        GameManager.Instance.AddMoney(-LevelSelector.Instance.Level_List[levelID].UnlockCost);
        GameManager.Instance.Lvl++;
        LevelCanvasManager.Instance.UpdateLevels();
    }

    public void SaveDatas()
    {
        GameManager.Instance.GetComponent<GameSaveData>().SaveDatas();
    }

    private void OnDestroy()
    {
        LevelCanvasManager.Instance.LvlPanel_List.Remove(gameObject);
    }
}
