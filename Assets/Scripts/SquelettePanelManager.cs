﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NaughtyAttributes;


public class SquelettePanelManager : MonoBehaviour
{

    public GameObject P_SquelettePanel;
    public List<PiedestalScript> SquelettePanel_List;

    public List<SqueletteSave> sqlSave;

    string filePath = "/musee.dat";

    //Start is called before the first frame update
    void Start()
    {
        LoadDatas();   
    }

    public void LoadDatas()
    {
        if (File.Exists(Application.persistentDataPath + filePath))//si la sauvegarde existe
        {
            BinaryFormatter decoder = new BinaryFormatter();
            FileStream stream = File.OpenRead(Application.persistentDataPath + filePath);
            List<SqueletteSave> data = (List<SqueletteSave>)decoder.Deserialize(stream);

            //on charge le tableau des scores
            sqlSave = data;

            stream.Close();
            Debug.Log(filePath + " : Sauvegarde chargée");
        }
        else
        {
            Debug.Log("\"" + filePath + "\" don't exist");
            for (int i = 0; i < SqueletteData.Instance.squelette.Count; i++)
            {

                sqlSave.Add(new SqueletteSave());
                Debug.Log(sqlSave[i] == null);
                sqlSave[i].pieceReveal = new bool[SqueletteData.Instance.squelette[i].v2_fossileRequired.Length];
            }
        }



        for (int i = 0; i < SqueletteData.Instance.squelette.Count; i++)
        {
            if (i < SquelettePanel_List.Count)//mettre a jour les piedestals existants
            {
                SquelettePanel_List[i].SetUpPiedestal(i, sqlSave[i].pieceReveal);

            }
            else//Instancier un nouveau piedestal
            {
                GameObject instance = Instantiate(P_SquelettePanel, transform);
                instance.GetComponent<PiedestalScript>().SetUpPiedestal(i, sqlSave[i].pieceReveal);
                SquelettePanel_List.Add(instance.GetComponent<PiedestalScript>());
            }
            
        }
    }

    [Button]
    public void DeleteDatas()
    {
        Debug.Log(filePath + " deleted");
        File.Delete(Application.persistentDataPath + filePath);
    }

    public void SaveDatas()
    {
        for (int i = 0; i < sqlSave.Count; i++)
        {
            sqlSave[i].pieceReveal = SquelettePanel_List[i].piecePart;
        }

                try
        {
            //sauvegarde
            List<SqueletteSave> data;
            data = sqlSave;

            BinaryFormatter encoder = new BinaryFormatter();
            FileStream stream = File.OpenWrite(Application.persistentDataPath + filePath);
            encoder.Serialize(stream, data);
            stream.Close();
            Debug.Log(filePath+"Sauvegarde Réussie");
        }
        catch (System.Exception)
        {
            Debug.LogWarning(filePath + " : la sauvegarde a eu un problème");
        }

        GameManager.Instance.GetComponent<GameSaveData>().SaveDatas();
    }


    public void ResetDatas()
    {
        for(int i = 0; i < sqlSave.Count; i++)
        {
            for(int j = 0; j < sqlSave[i].pieceReveal.Length; i++)
            {
                sqlSave[i].pieceReveal[j] = false;
            }
        }
        SaveDatas();
        LoadDatas();
    }

}

[System.Serializable]
public class SqueletteSave
{
    public bool[] pieceReveal;
}
