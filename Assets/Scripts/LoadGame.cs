﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadGame : MonoBehaviour
{
    public List<GameObject> Go_Managers;

    private void Awake()
    {
        foreach(GameObject go in Go_Managers)
        {
            Instantiate(go, Vector3.zero, Quaternion.identity);
        }
    }
}
