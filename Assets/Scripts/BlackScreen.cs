﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BlackScreen : MonoBehaviour
{
    public static BlackScreen Instance;
    public Image screen;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        screen.color = new Color(0,0,0,1);//noir avec alpha
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
