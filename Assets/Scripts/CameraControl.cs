﻿using System.Collections;
using UnityEngine;
using DG.Tweening;


public class CameraControl : MonoBehaviour
{
    public Vector3 T_target = new Vector3(0,-8,-10);
    public Vector3 T_target2 = new Vector3(17, 0, -10);
    public Transform InitialWayPoint, MuseeWayPoint;
    public int menuType=1;

    bool B_DisableMenuUpdate;
    // Start is called before the first frame update
    void Start()
    {
        if (GameManager.Instance.HasStartGame) {
            //Après lancement du jeu quand on reviens au menu on n'a plus le traveling
            StartCoroutine(MoveCamera(0, InitialWayPoint.position));
            BlackScreen.Instance.screen.DOFade(0, 0.5f);   
        }
        else
        {
            //Au lancement du jeu on a le droit à un petit traveling de 4sec
            StartCoroutine(MoveCamera_Cor(0, InitialWayPoint.position, 4f));
            GameManager.Instance.HasStartGame = true;
            BlackScreen.Instance.screen.DOFade(0, 0.5f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (B_DisableMenuUpdate) { return; }
        if (menuType == 1)
        {


            T_target += new Vector3(0, Input.mouseScrollDelta.y, 0f);

            if (T_target.y > -8)
            {
                T_target.y = -8;
            }
            else if (T_target.y < -45)
            {
                T_target.y = -45;
            }

            Camera.main.transform.position += (T_target - Camera.main.transform.position) * 0.1f;
            if (Vector3.Distance(Camera.main.transform.position, T_target) < 0.05f)
            {
                Camera.main.transform.position = T_target;
            }
        }
           
        

    }

    public void SwitchMenu(int i)
    {
        B_DisableMenuUpdate = true;
        menuType = 0;

        switch (i)
        {
            case 0:
                StartCoroutine(MoveCamera_Cor(i, InitialWayPoint.position));
                break;

            case 1:
                T_target = new Vector3(0, -8, -10);
                StartCoroutine(MoveCamera_Cor(i, T_target));

                break;

            case 2:
                StartCoroutine(MoveCamera_Cor(i, MuseeWayPoint.position));
                break;

            case 3:
                T_target = new Vector3(17, 0, -10);
                StartCoroutine(MoveCamera_Cor(i, MuseeWayPoint.position));
                break;
        }
        
    }

    IEnumerator MoveCamera(int i,Vector3 newVect)
    {
        MenuCanvas.Instance.CloseMenu();
        transform.position = newVect;
        yield return new WaitForSeconds(0.5f);
        MenuCanvas.Instance.OpenMenu(i);
        B_DisableMenuUpdate = false;
        menuType = i;
    }

    IEnumerator MoveCamera_Cor(int i, Vector3 newVect, float duration = 1.5f, Ease easeType = Ease.InOutSine)
    {
        MenuCanvas.Instance.CloseMenu_WithCoroutine();
        yield return transform.DOMove(newVect, duration).SetEase(easeType).WaitForCompletion() ;
        MenuCanvas.Instance.OpenMenu(i);
        B_DisableMenuUpdate = false;
        menuType = i;
    }

    public void SaveDatas()
    {
        GameManager.Instance.GetComponent<GameSaveData>().SaveDatas();
    }
}
