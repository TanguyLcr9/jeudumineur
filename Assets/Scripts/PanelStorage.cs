﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelStorage : MonoBehaviour
{
    public int ID;
    public Image image;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI countText;
    public TextMeshProUGUI sellText;
    public Fossile fossile;


    public void SetPanel(int id)
    {
        fossile = FossileData.Instance.Fossiles_List[id];
        ID = id;
        image.sprite = fossile.Sprite;
        nameText.text = fossile.Name;
        sellText.text = "Sell : "+fossile.SellValue+"$";
    }

    public void UpdatePanel()
    {
        if (GameManager.Instance.I_NumberTresor_Array[ID] == 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
            countText.text = "x " + GameManager.Instance.I_NumberTresor_Array[ID];
        }
    }

    public void SellItem()
    {
        GameManager.Instance.I_NumberTresor_Array[ID]--;
        GameManager.Instance.AddMoney(fossile.SellValue);
        UpdatePanel();
    }
}
