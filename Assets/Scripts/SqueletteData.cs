﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Squelette
{
    public string name;
    public string description;
    public Sprite[] sprite_List = new Sprite[4];// découpage en 4 sprite d'un seul fossile
    public Vector2Int[] v2_fossileRequired = new Vector2Int[4];// premier chiffre = le type de fossile; deuxieme = nombre réquis
}

public class SqueletteData : MonoBehaviour
{
    public static SqueletteData Instance;
    public List<Squelette> squelette;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }
}
