﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StorageContent : MonoBehaviour
{
    public GameObject Go_MuseePanel;
    public Transform DummyEnd, DummyStart;

    private void Start()
    {
        Go_MuseePanel.transform.position = DummyEnd.position;
    }

    public void MoveStoragePanel(bool State)
    {
        foreach (PanelStorage panels in MenuCanvas.Instance.Panel_Ar)
        {
            panels.UpdatePanel();
        }
        Go_MuseePanel.transform.DOMove(State ? DummyEnd.position : DummyStart.position, 0.5f);
    }
}
