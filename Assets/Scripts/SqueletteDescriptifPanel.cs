﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;




public class SqueletteDescriptifPanel : MonoBehaviour
{
    public TextMeshProUGUI Title, Description;
    public SqueletteData datas;
    public List<GameObject> RequirementBu;
    public int ID;
    public GameObject SelectedPiedestal;
    Squelette actualSquel;
    // Start is called before the first frame update
    void Start()
    {
        datas = SqueletteData.Instance;
    }

    public void SetUpPanel(int id,GameObject Piedestal) {
        ID = id;
        SelectedPiedestal = Piedestal;
        actualSquel = datas.squelette[ID];

        if (true)
        {
            Title.text = "???";
            Description.text = "?????";
        }
        else {
            Title.text = actualSquel.name;
            Description.text = actualSquel.description;
        }

        for(int i = 0; i < RequirementBu.Count; i++) {
            RequirementBu[i].GetComponent<FosRequire>().SetButton(actualSquel.v2_fossileRequired[i],Piedestal.GetComponent<PiedestalScript>().piecePart[i]);
        }
    }

    public void BuyAction(int numero) {

        GameManager.Instance.I_NumberTresor_Array[actualSquel.v2_fossileRequired[numero].x] -= actualSquel.v2_fossileRequired[numero].y;

        SelectedPiedestal.GetComponent<PiedestalScript>().RevealSquelettePart(numero);
        SetUpPanel(ID, SelectedPiedestal);
    }
}
