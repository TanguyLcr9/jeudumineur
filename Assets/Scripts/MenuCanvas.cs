﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using TMPro;
using System.Collections.Generic;



public class MenuCanvas : MonoBehaviour
{

    public static MenuCanvas Instance;

    [Header("Canvas")]
    public GameObject Go_Menu;
    public GameObject Go_LevelSelector;
    public GameObject Go_Museum;
    public GameObject Go_MuseumStorage;
    public GameObject Go_LevelWorldCanvas;
    public GameObject Go_MuseeCanvas;
    public GameObject Go_ContentStorage;
    public GameObject Go_PanelSquelette;
    public GameObject Go_LevelSelectionMenu;

    [Header("Prefabs")]
    public GameObject P_StoragePanel;

    [Header("Arrays")]
    public PanelStorage[] Panel_Ar;

    public void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void Start()//pour s'assurer qu'on a bien uniquement le menu zero au lancement
    {
        Go_Menu.SetActive(false);
        Go_LevelSelector.SetActive(false);
        Go_LevelWorldCanvas.SetActive(false);
        Go_Museum.SetActive(false);
        Go_MuseumStorage.SetActive(false);
        Go_MuseeCanvas.SetActive(false);
        Go_LevelSelectionMenu.SetActive(false);
        Go_PanelSquelette.SetActive(false);
        Panel_Ar = new PanelStorage[FossileData.Instance.Fossiles_List.Count];
        for(int i = 0; i < FossileData.Instance.Fossiles_List.Count; i++)
        {
            GameObject newPanel = Instantiate(P_StoragePanel, Vector3.zero, Quaternion.identity,Go_ContentStorage.transform);

            Panel_Ar[i] = newPanel.GetComponent<PanelStorage>();
            newPanel.GetComponent<PanelStorage>().SetPanel(i);
        }
        
    }

    public void CloseMenu()
    {
        Go_Menu.SetActive(false);
        Go_LevelSelector.SetActive(false);
        Go_LevelWorldCanvas.SetActive(false);
        Go_Museum.SetActive(false);
        Go_MuseumStorage.SetActive(false);
        Go_MuseeCanvas.SetActive(false);
        Go_LevelSelectionMenu.SetActive(false);
        Go_PanelSquelette.SetActive(false);
    }


        public void CloseMenu_WithCoroutine()
    {
        StartCoroutine(SetInterCanvas(Go_Menu, false));
        StartCoroutine(SetInterCanvas(Go_LevelSelector, false));
        StartCoroutine(SetInterCanvas(Go_LevelWorldCanvas, false));
        StartCoroutine(SetInterCanvas(Go_Museum, false));
        StartCoroutine(SetInterCanvas(Go_MuseumStorage, false));
        StartCoroutine(SetInterCanvas(Go_MuseeCanvas, false));
        StartCoroutine(SetInterCanvas(Go_LevelSelectionMenu, false));
        StartCoroutine(SetInterCanvas(Go_PanelSquelette, false));
    }

    public void OpenMenu(int i)
    {
        switch (i)
        {
            case 0://Menu Base
                StartCoroutine(SetInterCanvas(Go_Menu, true));
                break;

            case 1://Forage Level Selection
                StartCoroutine(SetInterCanvas(Go_LevelSelector, true));
                StartCoroutine(SetInterCanvas(Go_LevelWorldCanvas, true));
                break;

            case 2://Museum Place
                StartCoroutine(SetInterCanvas(Go_Museum, true));
                break;

            case 3://Museum Interior
                foreach(PanelStorage panels in Panel_Ar)
                {
                    panels.UpdatePanel();
                }
                StartCoroutine(SetInterCanvas(Go_MuseeCanvas, true));
                StartCoroutine(SetInterCanvas(Go_MuseumStorage, true));
                break;
            case 4://panel Squelette
                
                StartCoroutine(SetInterCanvas(Go_MuseeCanvas, true));
                break;
        }
    }

    public void OpenLevelDescription(int level)
    {

        OpenSelectorMenu(level, true);
    }

    public void CloseLevelDescription()
    {
        OpenSelectorMenu(0, false);
    }

    public void OpenSelectorMenu(int level,bool state)
    {
        StartCoroutine(SetInterCanvas(Go_LevelSelectionMenu, state));
        StartCoroutine(SetInterCanvas(Go_LevelWorldCanvas, !state, false));
        if (state)
        {
            Go_LevelSelectionMenu.GetComponent<LevelSelector>().SelectionLevel(level);
        }
        //Go_LevelWorldCanvas.GetComponent<CanvasGroup>().interactable = !state;
    }


    public void CloseSquelettePanel()
    {
        OpenSquelettePanel(false);
    }

    public void OpenSquelettePanel(bool state, int ID = 0, GameObject Piedestal = null)
    {
        if(state)
        Go_PanelSquelette.GetComponent<SqueletteDescriptifPanel>().SetUpPanel(ID, Piedestal);//set up le panel

        //affiche ou désaffiche en fonction de l'état
        StartCoroutine(SetInterCanvas(Go_PanelSquelette, state));
        Go_Museum.GetComponent<CanvasGroup>().interactable = !state;
        StartCoroutine(SetInterCanvas(Go_MuseumStorage, !state));
    }



    IEnumerator SetInterCanvas(GameObject go, bool state, bool affectGO = true)
    {
        if (state)
        {
            go.SetActive(true);
        }

        if (!state)
        {
            go.GetComponent<CanvasGroup>().interactable = false;
        }

        if (affectGO)
        {
            yield return go.GetComponent<CanvasGroup>().DOFade(state ? 1 : 0, 1f).WaitForCompletion();
        }
        else
        {
            yield return new WaitForSeconds(1f);
        }

        if (!state && affectGO)
        {
            go.SetActive(false);
        }

        if (state)
        {
            go.GetComponent<CanvasGroup>().interactable = true;
        }

        yield return null;

    }

}
