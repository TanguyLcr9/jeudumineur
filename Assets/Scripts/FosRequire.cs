﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FosRequire : MonoBehaviour
{
    public TextMeshProUGUI number;
    public Image image;

    public Image fond_Image;


    public void SetButton(Vector2Int requireNumber, bool Hide)
    {
        if(Hide || requireNumber.y == 0)
        {
            gameObject.SetActive(false);
            return;
        }
        else
        {
            gameObject.SetActive(true);
        }

        image.sprite = FossileData.Instance.Fossiles_List[requireNumber.x].Sprite;
        number.text = GameManager.Instance.I_NumberTresor_Array[requireNumber.x] + "/" + requireNumber.y;

        if(GameManager.Instance.I_NumberTresor_Array[requireNumber.x] >= requireNumber.y)
        {
            fond_Image.color = gameObject.GetComponent<Button>().colors.normalColor;
            gameObject.GetComponent<Button>().interactable = true;
        }
        else
        {
            fond_Image.color = gameObject.GetComponent<Button>().colors.disabledColor;
            gameObject.GetComponent<Button>().interactable = false;
        }
    }


}
