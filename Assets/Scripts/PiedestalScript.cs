﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PiedestalScript : MonoBehaviour
{
    public int ID;
    public Image[] squeletteParts_List;
    public Image PiedestalImage;
    public bool[] piecePart;


    public void SetUpPiedestal(int id, bool[] save)
    {
        ID = id;
        int partCount = SqueletteData.Instance.squelette[ID].sprite_List.Length;
        piecePart = save;
        for (int i = 0; i < partCount; i++)
        {
            squeletteParts_List[i].sprite = SqueletteData.Instance.squelette[ID].sprite_List[i];
            squeletteParts_List[i].gameObject.SetActive(piecePart[i]);
        }
    }

    public void RevealSquelettePart(int part)
    {
        squeletteParts_List[part].gameObject.SetActive(true);
        piecePart[part] = true;
    }

    /// <summary>
    /// Pour que le bouton affiche le panel
    /// </summary>
    public void ShowPanel()
    {
        MenuCanvas.Instance.OpenSquelettePanel(true,ID,gameObject);
    }
}
