﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class OreBehaviour : TresorBehaviour
{


    public void SetId(int pos,int id)
    {
        I_Type = id;
        Xslot = 1;
        Yslot = 1;
        SpriteRend.sprite = OresData.Instance.Ores_Ar[I_Type].Sprite;
        base.InitTresor(pos);
    }

    public override IEnumerator OnTresorFinded()
    {
        Transform t_spriteTransform = SpriteRend.transform;
        SpriteRend.sortingOrder = 3;
        InterfaceManager im = InterfaceManager.Instance;

        yield return t_spriteTransform.DOScale(transform.localScale + Vector3.one * 0.7f, 0.5f)
            .SetEase(Ease.InOutSine)
            .WaitForCompletion();

        Tween doScaleYoyo = t_spriteTransform.DOScale(transform.localScale + Vector3.one * 0.6f, 0.5f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

        yield return new WaitForSeconds(1f);

        yield return t_spriteTransform.DOMove(Camera.main.ScreenToWorldPoint(im.Bu_ToolBox.transform.position) + Vector3.forward * 10, 1f)
            .SetEase(Ease.InQuart)
            .WaitForCompletion();

        doScaleYoyo.Kill();
        im.Bu_Chest.transform.DOScale(Vector3.one * 0.5f, 0.2f).SetLoops(2, LoopType.Yoyo);


        LevelManager.Instance.CollectOre(1, I_Type);
        InterfaceManager.Instance.UpdateTexts();
        InterfaceManager.Instance.UpdateChestMenu();
        yield return t_spriteTransform.DOScale(Vector3.zero, 0.5f).WaitForCompletion();


    }


}
