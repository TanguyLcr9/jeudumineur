﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BlocBehaviour : MonoBehaviour
{
    public int Index;
    public int I_HpBloc;
    public bool B_tresorSlot;
    public Collider2D Col2D_Box;
    public SpriteRenderer SprRend_Box;
    public SpriteRenderer SprRend_Craq;
    public Sprite Spr_Ore;
    public GameObject Go_Contours;
    public bool B_IsMined;
    public List<Sprite> Spr_Craquelures = new List<Sprite>();

    public int I_HpBlocMax;
    public Color Col_Hide;
    public Color Col_Show;
    public Color Col_Over;

    bool isShowing;
    // Start is called before the first frame update
    void Start()
    {
        Col2D_Box = GetComponent<Collider2D>(); //Collider à coté maintenant
        //SprRend_Box = GetComponent<SpriteRenderer>();
        SprRend_Box.color = Col_Hide;
        Go_Contours.SetActive(false);

        int Ran = Random.Range(0, 4);

        SprRend_Box.transform.eulerAngles = new Vector3(0, 0, 0);
        I_HpBlocMax = I_HpBloc;


    }


    private void OnMouseOver()
    {

        if (LevelManager.Instance.B_DisableInteraction || LevelManager.Instance.B_LevelEnd) { OnMouseExit(); return; }
        OnOverByTypeOfTool(true);


    }

    private void OnMouseExit()
    {
        OnOverByTypeOfTool(false);
    }

    public void OnOverByTypeOfTool(bool state)// methode pour over les autres blocs en fonction du type d'outils
    {
        List<BlocBehaviour> blocs = BoardManager.Instance.Bloc_List;
        BoardManager board = BoardManager.Instance;
        int type = LevelManager.Instance.I_ToolType;

        switch (type)
        {
            case 0:
                OnOver(state);
                break;

            case 1:

                if (Index - board.I_BlocX > 0 && !blocs[Index - board.I_BlocX].B_IsMined)
                    blocs[Index - board.I_BlocX].OnOver(state);//bloc du dessous

                if (Index + board.I_BlocX < blocs.Count && !blocs[Index + board.I_BlocX].B_IsMined)
                    blocs[Index + board.I_BlocX].OnOver(state);//bloc du dessus


                goto case 0;

            case 2:
            case 3:

                if ((Index) % board.I_BlocX != 0 && !blocs[Index - 1].B_IsMined)
                    blocs[Index - 1].OnOver(state);//bloc de gauche

                if ((Index) % board.I_BlocX != board.I_BlocX - 1 && !blocs[Index + 1].B_IsMined)
                    blocs[Index + 1].OnOver(state);//bloc de droite

                if (type == 3)
                    goto case 1;

                goto case 0;
        }
    }

    public void OnShowByTypeOfTool()// methode pour over les autres blocs en fonction du type d'outils
    {
        List<BlocBehaviour> blocs = BoardManager.Instance.Bloc_List;
        BoardManager board = BoardManager.Instance;
        int tool = LevelManager.Instance.I_ToolType;
        switch (tool)
        {
            case 0:
                ShowOre();
                break;

            case 1:

                if (Index - board.I_BlocX > 0 && !blocs[Index - board.I_BlocX].B_IsMined)
                    blocs[Index - board.I_BlocX].ShowOre();//bloc du dessous

                if (Index + board.I_BlocX < blocs.Count && !blocs[Index + board.I_BlocX].B_IsMined)
                    blocs[Index + board.I_BlocX].ShowOre();//bloc du dessus

                goto case 0;

            case 2:
            case 3:

                if ((Index) % board.I_BlocX != 0 && !blocs[Index - 1].B_IsMined)
                    blocs[Index - 1].ShowOre();

                if ((Index) % board.I_BlocX != board.I_BlocX - 1 && !blocs[Index + 1].B_IsMined)
                    blocs[Index + 1].ShowOre();

                if (tool == 3)
                    goto case 1;

                goto case 0;

        }
    }

    public void OnOver(bool state = true)// quand on passe l'outil dessus [JUICE FONCTION]
    {
        if (B_IsMined) { return; }
        Go_Contours.SetActive(state);// les contours se montrent ou se cache
        SprRend_Box.sortingOrder = state ? 2 : 0;// afin de l'afficher un cran au dessus des autres
        SprRend_Craq.sortingOrder = state ? 3 : 1;// afin de l'afficher un cran au dessus des autres
    }



    private void OnMouseDown()
    {
        if (LevelManager.Instance.B_DisableInteraction || LevelManager.Instance.B_LevelEnd) { return; }
        OnShowByTypeOfTool();
        StartCoroutine(LevelManager.Instance.UsingTool(this));

    }



    public void ShowOre()
    {

        if (B_tresorSlot)
        {
            SprRend_Box.sprite = Spr_Ore;
        }
        SprRend_Box.DOColor(Col_Show, 0.5f);
        isShowing = true;
    }

    public void MinedOres()
    {

        if (B_IsMined) { return; }
        if (I_HpBloc > 0)
        {

            I_HpBloc--;
            
            InterfaceManager.Instance.UpdateTexts();
            if(LevelManager.Instance.I_Solidity< LevelManager.Instance.I_BlocDestroyed)
            {
                LevelManager.Instance.EndLevel();
            }
            else
            {
                LevelManager.Instance.I_BlocDestroyed++;
            }
            SprRend_Craq.sprite = Spr_Craquelures[Mathf.FloorToInt(Mathf.Lerp(Spr_Craquelures.Count - 1, 0, (float)I_HpBloc / I_HpBlocMax))];
            SprRend_Craq.sortingOrder = 1;

        }
        else
        {
            B_IsMined = true;
            SprRend_Box.enabled = false;
            SprRend_Craq.enabled = false;
            BoardManager.Instance.CheckAndShowOres(Index);
        }

    }


}
