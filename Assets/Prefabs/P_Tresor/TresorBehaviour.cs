﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TresorBehaviour : MonoBehaviour
{
    [Header("Type ID :")]
    public int I_Type;
    [HideInInspector]
    public int I_pos;
    //[HideInInspector]
    //public int Xpos, Ypos;
    public int Xslot, Yslot;

    [HideInInspector]
    public bool B_IsFound;

    public SpriteRenderer SpriteRend;

    public virtual void SetId(int id)
    {
        I_Type = id;
        FossileData Fdata = FossileData.Instance;
        Debug.Log(id);
        if ((int)Fdata.Fossiles_List[id].Slots.x != 0)
        {
            
            Xslot = (int)Fdata.Fossiles_List[id].Slots.x;
            Debug.Log(id+ "isGood");
        }
        else
        {
            Debug.LogWarning("X is not set for the fossile " + id);
        }

        if((int)Fdata.Fossiles_List[id].Slots.y != 0)
        {
            Yslot = (int)Fdata.Fossiles_List[id].Slots.y;
        }
        else
        {
            Debug.LogWarning("Y is not set for the fossile " + id);
        }
        
        if (Fdata.Fossiles_List[id].Sprite != null)
        {
            SpriteRend.sprite = Fdata.Fossiles_List[id].Sprite;
        }
        else
        {
            Debug.LogWarning("There is no Image for the fossile " + id);
        }
    }

    // Start is called before the first frame update
    public virtual void InitTresor(int pos)
    {
        BoardManager board = BoardManager.Instance;
        SpriteRend = GetComponentInChildren<SpriteRenderer>();
        SpriteRend.sortingOrder = -1;
        SpriteRend.transform.position += new Vector3(Xslot / 2f,Yslot/2f,0f);

        I_pos = pos;

        for (int x = 0; x < Xslot; x++)
        {
            for (int y = 0; y < Yslot; y++)
            {
                if (I_pos + x + y * board.I_BlocX < board.Bloc_List.Count)
                {
                    board.Bloc_List[I_pos + x + y * board.I_BlocX].B_tresorSlot = true;
                }
                else Debug.LogError("Index out of range in blocs list");
            }
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CheckIfDiscovered()
    {
        if (B_IsFound) { return; }
        BoardManager board = BoardManager.Instance;

        for(int x = 0; x < Xslot; x++)
        {
            for (int y = 0; y < Yslot; y++)
            {
                if (!board.Bloc_List[I_pos + x + y * board.I_BlocX].B_IsMined)
                {
                    return;
                }
            }
        }

        B_IsFound = true;
        StartCoroutine(OnTresorFinded());
    }

    public virtual IEnumerator OnTresorFinded()
    {
        Transform t_spriteTransform = SpriteRend.transform;
        SpriteRend.sortingOrder = 3;
        InterfaceManager im = InterfaceManager.Instance;

        yield return t_spriteTransform.DOScale(transform.localScale + Vector3.one * 0.7f, 0.5f)
            .SetEase(Ease.InOutSine)
            .WaitForCompletion();

        Tween doScaleYoyo = t_spriteTransform.DOScale(transform.localScale + Vector3.one * 0.6f, 0.5f)
            .SetLoops(-1,LoopType.Yoyo)
            .SetEase(Ease.InOutSine);

        yield return new WaitForSeconds(1f);

        yield return t_spriteTransform.DOMove(Camera.main.ScreenToWorldPoint(im.Bu_Chest.transform.position)+Vector3.forward*10, 1f)
            .SetEase(Ease.InQuart)
            .WaitForCompletion();

        doScaleYoyo.Kill();
        im.Bu_Chest.transform.DOScale(Vector3.one * 0.5f, 0.2f).SetLoops(2, LoopType.Yoyo);


        LevelManager.Instance.CollectTresor(1, I_Type);
        InterfaceManager.Instance.UpdateTexts();
        InterfaceManager.Instance.UpdateChestMenu();
        yield return t_spriteTransform.DOScale(Vector3.zero, 0.5f).WaitForCompletion();

    }
}
