﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolBehaviour : MonoBehaviour
{
    public GameObject Go_Tool;
    public ParticleSystem Particles;

    Animator tool_Animator;
   

    public void Start()
    {
        tool_Animator = Go_Tool.GetComponent<Animator>();
    }

    public void UseTool()
    {
        var em = Particles.emission;
            em.enabled = true;
        tool_Animator.SetTrigger("Mining");
    }

    public void InitTool()
    {
        var em = Particles.emission;
        em.enabled = false;
    }

    private void Update()
    {
        if (LevelManager.Instance.B_DisableInteraction) { return; }
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0,0,10);

    }

}
