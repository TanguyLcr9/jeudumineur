﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NaughtyAttributes;

public class GameSaveData : MonoBehaviour
{
    [HideInInspector()]
    public GameDatas gameDatas;
    string filePath = "/game.dat";

    // Start is called before the first frame update
    void Start()
    {
        InitialiseDatas();

        LoadDatas();
        
    }

    public void LoadDatas()
    {
        if (File.Exists(Application.persistentDataPath + filePath))//si la sauvegarde existe
        {
            BinaryFormatter decoder = new BinaryFormatter();
            FileStream stream = File.OpenRead(Application.persistentDataPath + filePath);
            GameDatas data = (GameDatas)decoder.Deserialize(stream);

            //on charge le tableau des scores
            gameDatas = data;

            stream.Close();

            GameManager GM = GameManager.Instance;

            GM.I_NumberTresor_Array = gameDatas.tresors;
            GM.I_NumberOre_Array = gameDatas.ores;

            GM.SetMoney(gameDatas.money);
            GM.Lvl = gameDatas.lvl;
            Debug.Log(filePath + " : Sauvegarde chargée");
        }
        else
        {
            Debug.Log("\"" + filePath + "\" don't exist");
        }
    }

    public void SaveDatas()
    {
        GameManager GM = GameManager.Instance;

        gameDatas.tresors = GM.I_NumberTresor_Array;
        gameDatas.ores = GM.I_NumberOre_Array;

        gameDatas.money = GM.I_Money;
        gameDatas.lvl = GM.Lvl;

        try
        {
            //sauvegarde
            GameDatas data;
            data = gameDatas;
            

            BinaryFormatter encoder = new BinaryFormatter();
            FileStream stream = File.OpenWrite(Application.persistentDataPath + filePath);
            encoder.Serialize(stream, data);
            stream.Close();
            Debug.Log(filePath+" : Sauvegarde Réussie");
        }
        catch (System.Exception)
        {
            Debug.LogWarning(filePath + " : la sauvegarde a eu un problème");
        }
    }

    /// <summary>
    /// Initialise les données
    /// </summary>
    public void InitialiseDatas()
    {
        gameDatas.tresors = new int[FossileData.Instance.Fossiles_List.Count];
        gameDatas.ores = new int[FossileData.Instance.Fossiles_List.Count];

        gameDatas.money = 2200;
        gameDatas.lvl = 1;
    }

    /// <summary>
    /// Remets à l'intial les données du jeu, étape destructive
    /// </summary>
    public void ResetDatas()
    {
        gameDatas.tresors = new int[FossileData.Instance.Fossiles_List.Count];
        gameDatas.ores = new int[FossileData.Instance.Fossiles_List.Count];

        gameDatas.money = 2200;
        gameDatas.lvl = 1;
        try
        {
            //sauvegarde
            GameDatas data;
            data = gameDatas;


            BinaryFormatter encoder = new BinaryFormatter();
            FileStream stream = File.OpenWrite(Application.persistentDataPath + filePath);
            encoder.Serialize(stream, data);
            stream.Close();
            Debug.Log(filePath + " : Sauvegarde Réussie");
        }
        catch (System.Exception)
        {
            Debug.LogWarning(filePath + " : la sauvegarde a eu un problème");
        }
    }

    [Button]
    public void DeleteDatas()
    {
        Debug.Log(filePath + " deleted");
        File.Delete(Application.persistentDataPath + filePath);
    }

}



[System.Serializable]
public class GameDatas
{
    public int[] tresors;
    public int[] ores;

    public int money;
    public int lvl;
}