﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using DG.Tweening;
using NaughtyAttributes;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public int[] I_NumberTresor_Array;
    public int[] I_NumberOre_Array;

    public int I_Money;
    public int Lvl=1;

    public TextMeshProUGUI TMP_MoneyCount;
    public CanvasGroup UI_General_Group;

    public bool HasStartGame;
    

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
        ResetData();
    }

    private void ResetData()
    {

        I_NumberTresor_Array = new int[FossileData.Instance.Fossiles_List.Count];
        I_NumberTresor_Array.Initialize();
        
    }

    public void ShowMoneyCount(bool state)
    {
        UI_General_Group.alpha = state ? 1 : 0;
    }

    IEnumerator ShowMoneyCount_Cor(bool state = true)
    {
        yield return UI_General_Group.DOFade(state ? 1 : 0, 1f).WaitForCompletion();
    }

    public void AddMoney(int addCount)
    {
        SetMoney(I_Money+addCount);
    }

    public void SetMoney(int moneyValue)
    {
        I_Money = moneyValue;
        TMP_MoneyCount.text = "" + I_Money + "$";
    }

    /// <summary>
    /// Choisir le numero du level a charger et déduire un cout
    /// </summary>
    /// <param name="i"></param>
    public void LoadLevel(int i, int cost)
    {
        AddMoney(-cost); 
        SceneManager.LoadScene(i);
    }

    [Button]
    public void GiveExtra()
    {
        for (int i = 0;i<I_NumberTresor_Array.Length;i++)
        {
            I_NumberTresor_Array[i] += 100;
        }

        for (int i = 0; i < I_NumberOre_Array.Length; i++)
        {
            I_NumberOre_Array[i] += 100;
        }

        AddMoney(10000);
    }

    [Button]
    public void Zero()
    {
        for (int i = 0; i < I_NumberTresor_Array.Length; i++)
        {
            I_NumberTresor_Array[i] = 0;
        }

        for (int i = 0; i < I_NumberOre_Array.Length; i++)
        {
            I_NumberOre_Array[i] = 0;
        }

        SetMoney(2000);
        Lvl = 1;
    }

    [Button]
    public void LevelRestore()
    {
        Lvl = 3;
    }
}
