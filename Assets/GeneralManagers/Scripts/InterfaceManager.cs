﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class InterfaceManager : MonoBehaviour
{
    public static InterfaceManager Instance;
    public TextMeshProUGUI[] Ore_Text;
    public Slider Solidity_Slider;
    public Image Solidity_Image;
    public Button Bu_Chest, Bu_ToolBox;

    public GameObject Go_MainMenu;
    public GameObject Go_ChestMenu;
    public GameObject Go_ToolBoxMenu;
    public GameObject Go_EndPanel;
    public GameObject Go_QuitPanel;
    public CanvasGroup Buttons;

    public GameObject P_FossileDetail;
    public Transform Content_Chest;

    public List<PanelTresor> panelTresors_List = new List<PanelTresor>();


    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);

        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        Go_ChestMenu.SetActive(false);
        Go_ToolBoxMenu.SetActive(false);
        //Go_ToolBoxMenu.SetActive(false);
        Go_MainMenu.SetActive(true);
        Go_EndPanel.SetActive(false);
        Go_QuitPanel.SetActive(false);

        InitChestMenu();

    }

    public void InitialiseInterface()
    {
        Solidity_Slider.maxValue = LevelManager.Instance.I_Solidity;
        Solidity_Slider.value = 0;
        UpdateTexts();
    }

    public void UpdateTexts()
    {
        LevelManager levelManager = LevelManager.Instance;
        Solidity_Slider.value = levelManager.I_BlocDestroyed;
        Solidity_Image.color = new Color(1f, 1f - (float)levelManager.I_BlocDestroyed / (float)levelManager.I_Solidity, 1f- (float)levelManager.I_BlocDestroyed / (float)levelManager.I_Solidity);
        
    }

    public void ShowMenu(int menuInt)
    {
        AffectMenu(true, menuInt);
    }

    public void CloseMenu()
    {
        AffectMenu(false, 0);
    }

    void AffectMenu(bool state, int menuInt)
    {
        switch (menuInt)
        {
            case 1:
                
                Go_ChestMenu.SetActive(state);
                break;
            case 2:

                Go_ToolBoxMenu.SetActive(state);
                break;
            case 3:
                Go_QuitPanel.SetActive(state);
                break;
            default:
                Go_ChestMenu.SetActive(state);
                Go_ToolBoxMenu.SetActive(state);
                break;
        }
        Go_MainMenu.SetActive(!state);
        Buttons.interactable = !state;
        LevelManager.Instance.B_DisableInteraction = state;
        Cursor.visible = state;
    }

    void InitChestMenu()
    {
        FossileData Fdata = FossileData.Instance;
        /*for(int i =0; i < GM.I_NumberTresor_Array.Length ; i++)
        {
            if (GM.I_NumberTresor_Array[i] > 0)
            {
                GameObject instance = Instantiate(P_FossileDetail);
                instance.transform.SetParent(Content_Chest);
                //instance.GetComponent<PanelTresor>().ID = ;
                panelTresors_List.Add(instance.GetComponent<PanelTresor>());
            }

        }*/
        for (int i = 0; i < Fdata.Fossiles_List.Count; i++)
        {
            GameObject newPanel = Instantiate(P_FossileDetail);
            newPanel.transform.SetParent(Content_Chest);
            newPanel.GetComponent<PanelTresor>().Initialise(i);
            newPanel.transform.localScale = new Vector3(1,1,1);//quickfix
            panelTresors_List.Add(newPanel.GetComponent<PanelTresor>());
        }
    }

    public void UpdateChestMenu()
    {
        foreach(PanelTresor panel in panelTresors_List)
        {
            panel.UpdateDatas();
        }
    }

    public void ShowEndPanel()
    {
        Go_EndPanel.SetActive(true);
        Go_EndPanel.GetComponent<CanvasGroup>().alpha = 0;
        Go_EndPanel.GetComponent<CanvasGroup>().DOFade(1, 1f);
        Go_ChestMenu.SetActive(false);
        Go_MainMenu.SetActive(false);
        Go_ToolBoxMenu.SetActive(false);
        Go_QuitPanel.SetActive(false);
    }

    public void ShowQuitPanel(bool state = true)
    {
        AffectMenu(state, 3);
    }

    public void QuitLevel()
    {
        LevelManager.Instance.QuitLevel();
    }
}
