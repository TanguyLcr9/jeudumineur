﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BoardManager : MonoBehaviour
{
     public static BoardManager Instance;

    public GameObject Go_Bloc;// il faudra enregistrer dans un tableau toutes les cellules du plateau que l'on créer
    public GameObject Go_Struct;
    public GameObject Go_BackgroundGrey;

    public List<GameObject> Go_OresBloc = new List<GameObject>();

    public GameObject Go_Bones;

    public Transform Dummy_Bloc , Dummy_Tresor , Dummy_Ores;

    public int I_BlocX = 5, I_BlocY = 4;
    public int I_TresorNumber, I_OreNumber;

    public List<BlocBehaviour> Bloc_List = new List<BlocBehaviour>();

    public GameObject Go_TresorBase;
    public List<TresorBehaviour> Tresor_List = new List<TresorBehaviour>();
    public List<OreBehaviour> Ore_List = new List<OreBehaviour>();

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);

        }
        else
        {
            Instance = this;
        }


    }

    public void InitialiseBlocs()
    {

        LevelManager GM = LevelManager.Instance;
        Go_Struct.GetComponent<SpriteRenderer>().size = new Vector2(I_BlocX + 1.2f, I_BlocY + 1.5f);

        Go_BackgroundGrey.GetComponent<SpriteRenderer>().size = new Vector2(I_BlocX, I_BlocY);

        for (int j = 0; j < I_BlocY; j++)//création du tableau de base en fonction de x et y
        {
            for (int i = 0; i < I_BlocX; i++)
            {
                GameObject newBloc = Instantiate(Go_Bloc, new Vector2(i - I_BlocX / 2f, j - I_BlocY / 2f), Quaternion.identity);
                newBloc.transform.SetParent(Dummy_Bloc);
                Bloc_List.Add(newBloc.GetComponentInChildren<BlocBehaviour>());
                Bloc_List[i + j * I_BlocX].Index = i + j * I_BlocX;
            }
        }

        for (int i = 0; i < I_TresorNumber; i++)
        {
            int ranPosition = 0;

            float ranOdd = Random.Range(0f, 100f);
            int ranTresor = 0;


            FossilesLocalData Fdata = FossilesLocalData.Instance;
            for (int j = 0; j < Fdata.I_typeNumber; j++)
            {
                if (j == 0)
                {
                    if (ranOdd > 0 && ranOdd <= Fdata.F_Proportions[j])
                    {
                        ranTresor = 0;
                    }
                }
                else
                {
                    if (ranOdd > Fdata.F_Proportions[j-1] && ranOdd <= Fdata.F_Proportions[j])
                    {
                        ranTresor = Fdata.I_TypeOfTresor[j];
                    }

                }
            }



            Go_Bones.GetComponent<TresorBehaviour>().SetId(ranTresor);

            TresorBehaviour tresor = Go_Bones.GetComponent<TresorBehaviour>();

            if (tresor.Xslot >= I_BlocX || tresor.Yslot >= I_BlocY) { Debug.LogError("Tresors are too big for the table"); break; }

            for (int w = 0; w <= 100; w++)
            {

                ranPosition = Random.Range(0, I_BlocX * I_BlocY);
                if (Bloc_List[ranPosition].B_tresorSlot) { continue; }
                bool positionIsGood = true;
                if (ranPosition + I_BlocX * (tresor.Yslot - 1) >= I_BlocX * I_BlocY || (ranPosition % I_BlocX >= I_BlocX - (tresor.Xslot - 1)))
                {// test pour savoir si le tresor passe dans le tableau, s'il dépasse ce n'est pas bon
                    positionIsGood = false;
                }
                else
                {
                    for (int x = 0; x <= tresor.Xslot; x++)
                    {
                        for (int y = 0; y <= tresor.Yslot; y++)
                        {
                            if (ranPosition + x + y * I_BlocX < I_BlocX * I_BlocY && Bloc_List[ranPosition + x + y * I_BlocX].B_tresorSlot)// test pour savoir si un tresor n'en survole pas un autre
                            {
                                positionIsGood = false;
                                break;
                            }
                        }
                        if (!positionIsGood) { break; }
                    }
                }
                if (w == 99)
                {
                    Debug.LogError("Too long to fit all the pieces, please change some parameters");
                    i = I_TresorNumber;
                    break;
                }

                if (positionIsGood) { break; }

                Debug.Log("* Change place of a tresor because was not fitting");
            }
            if (i == I_TresorNumber) { break; }

            GameObject new_Bones = Instantiate(Go_Bones, Bloc_List[ranPosition].transform.parent.transform.position, Quaternion.identity);
            new_Bones.transform.SetParent(Dummy_Tresor);
            Tresor_List.Add(new_Bones.GetComponent<TresorBehaviour>());
            Tresor_List[Tresor_List.Count - 1].InitTresor(ranPosition);
        }

        GM.I_TresorGoal = Tresor_List.Count;

        for (int i = 0; i < I_OreNumber; i++)
        {
            int ranPosition = 0;
            int ranOres = 0;
            for (int w = 0; w < 100; w++)
            {
                ranPosition = Random.Range(0, I_BlocX * I_BlocY);
                if (!Bloc_List[ranPosition].B_tresorSlot)
                {
                    break;
                }
                if (w == 99)
                {
                    i = I_OreNumber;
                }
            }
            if (i == I_OreNumber) { break; }

            GameObject new_Ore = Instantiate(Go_OresBloc[ranOres], Bloc_List[ranPosition].transform.parent.transform.position, Quaternion.identity);
            new_Ore.transform.SetParent(Dummy_Ores);
            new_Ore.GetComponent<OreBehaviour>().SetId(ranPosition,ranOres);
            Ore_List.Add(new_Ore.GetComponent<OreBehaviour>());
            
        }

        {
            /*if (I_OresNumber > I_BlocX * I_BlocY)// si plus de minerais que de blocs alors tout sera minerai
            {
                foreach (BlocBehaviour blocs in Bloc_List)
                {
                    blocs.B_IsOre = true;
                }
            }
            else
            {
                for (int i = 0; i < I_OresNumber; i++)//Fonction définissant aléatoirement les minerais dans le tableau
                {

                    int Ran;

                    while (true)
                    {
                        Ran = Random.Range(0, I_BlocX * I_BlocY);
                        if (!Bloc_List[Ran].B_IsOre)
                        {
                            break;
                        }
                    }

                    if (!Bloc_List[Ran].B_IsOre)
                    {
                        Bloc_List[Ran].B_IsOre = true;
                        numberOres++;
                    }


                    int Rand4 = Random.Range(0, 4);
                    switch (Rand4)
                    {
                        case 0:
                            if (Ran - 1 > 0 && !Bloc_List[Ran - 1].B_IsOre)
                            {
                                Bloc_List[Ran - 1].B_IsOre = true;
                                numberOres++;
                            }
                            break;
                        case 1:
                            if (Ran + 1 < I_BlocX * I_BlocY && Bloc_List[Ran + 1].B_IsOre)
                            {
                                Bloc_List[Ran + 1].B_IsOre = true;
                                numberOres++;
                            }
                            break;
                        case 2:
                            if (Ran > I_BlocX && !Bloc_List[Ran - I_BlocX].B_IsOre)
                            {
                                Bloc_List[Ran - I_BlocX].B_IsOre = true;
                                numberOres++;
                            }
                            break;
                        case 3:
                            if (Ran + I_BlocX < I_BlocX * I_BlocY && Bloc_List[Ran + I_BlocX].B_IsOre)
                            {
                                Bloc_List[Ran + I_BlocX].B_IsOre = true;
                                numberOres++;
                            }
                            break;
                    }

                }
            }*/
        }// fonction quand les minerais existaient (OBSOLETE)

        //GM.I_Solidity = GM.I_TresorGoal + (I_BlocX * I_BlocY - GM.I_TresorGoal) / 6;

        InterfaceManager.Instance.InitialiseInterface();
        Camera.main.orthographicSize = (I_BlocY + I_BlocX) / 3;
    }



    public void ResetTable()
    {
        foreach (BlocBehaviour blocs in Bloc_List)
        {
            Destroy(blocs.transform.parent.gameObject);

        }
        Bloc_List.Clear();

        foreach (TresorBehaviour tresor in Tresor_List)
        {
            Destroy(tresor.gameObject);
        }
        Tresor_List.Clear();

        foreach(OreBehaviour ores in Ore_List)
        {
            Destroy(ores.gameObject);

        }
        Ore_List.Clear();

        InitialiseBlocs();
    }

    public void CheckAndShowOres(int blocIndex)
    {
        for (int i = -1; i <= 1; i += 2)
        {
            if (blocIndex + i >= 0
                && blocIndex + i < I_BlocY * I_BlocX && Bloc_List[blocIndex + i] != null
                && (blocIndex % I_BlocX != 0 && i == -1)
                || (blocIndex % I_BlocX != I_BlocX - 1 && i == 1))
            {
                Bloc_List[blocIndex + i].ShowOre();
            }// check les blocs sur les côtés, et si il y a, alors montrer minerai

            if (blocIndex + I_BlocX * i >= 0
                && blocIndex + I_BlocX * i < I_BlocY * I_BlocX
                && Bloc_List[blocIndex + (I_BlocX * i)] != null) { Bloc_List[blocIndex + (I_BlocX * i)].ShowOre(); }// check les blocs sur le dessus dessous, et si il y a, alors montrer minerai
        }

    }
}
