﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FossilesLocalData : MonoBehaviour
{
    public static FossilesLocalData Instance;

    public int I_typeNumber;

    public List<int> I_TypeOfTresor = new List<int>();
    public List<float> F_Frequence = new List<float>();
    public List<float> F_Proportions = new List<float>();

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);

        }
        else
        {
            Instance = this;
        }

        CalculateFrequence();
    }

        public void CalculateFrequence()
    {
        float total = 0;
        foreach (float abondance in F_Frequence)
        {
            total += abondance;
        }
        for (int i = 0; i < F_Frequence.Count; i++)
        {
            F_Frequence[i] = (F_Frequence[i] * 100) / total;

        }

        F_Proportions[0] = F_Frequence[0];

        for (int i = 1; i < F_Proportions.Count; i++)
        {
            
                F_Proportions[i] = F_Proportions[i - 1] + F_Frequence[i];
            
        }
    }

    public void CreateCells()
    {
        if (I_typeNumber < I_TypeOfTresor.Count)
        {
            for (int i = I_TypeOfTresor.Count - 1; i >= I_typeNumber; i--)
            {
                I_TypeOfTresor.RemoveAt(i);
               

            }
        }
        else
        {
            for (int i = I_TypeOfTresor.Count; i < I_typeNumber; i++)
            {
                I_TypeOfTresor.Add(0);
            }
        }

        if (I_typeNumber < F_Frequence.Count)
        {
            for (int i = F_Frequence.Count - 1; i >= I_typeNumber; i--)
            {
                F_Frequence.RemoveAt(i);
                F_Proportions.RemoveAt(i);


            }
        }
        else
        {
            for (int i = F_Frequence.Count; i < I_typeNumber; i++)
            {
                F_Frequence.Add(0f);
                F_Proportions.Add(0f);
            }
        }

        }
}