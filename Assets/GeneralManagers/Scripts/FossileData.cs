﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[System.Serializable]
public class Fossile
{
    public string Name;
    public Vector2 Slots;
    [ShowAssetPreview(32,32)]
    public Sprite Sprite;
    public int SellValue;
}

/// <summary>
/// Class permettant de stocker les valeurs propres à chaques fossiles ainsi on les entre dans l'inspector (pourrait être une BDD)
/// </summary>
public class FossileData : MonoBehaviour
{
    public static FossileData Instance;

    public List<Fossile> Fossiles_List = new List<Fossile>();
    

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);

        }
        else
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        

    }


}
