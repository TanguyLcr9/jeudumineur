﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
#if UNITY_EDITOR_OSX
[CustomEditor(typeof(FossilesLocalData))]
public class FossilesLocalDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FossilesLocalData myScript = (FossilesLocalData)target;
        if (GUILayout.Button("Create Cells"))
        {
            myScript.CreateCells();
        }

        if (GUILayout.Button("Recalculate Frequence"))
        {
            myScript.CalculateFrequence();
        }
    }
}
#endif