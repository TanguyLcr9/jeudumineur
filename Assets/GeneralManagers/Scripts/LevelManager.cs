﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public int I_TresorCollected;
    public int I_OresCollected;
    public int I_Solidity;
    public int I_BlocDestroyed;
    public int I_TresorGoal;
    public int I_ToolType;
    public int[] I_NumberTresor_Array;
    public int[] I_NumberOre_Array;
    public RecapPanel recapPanel;

    public bool B_DisableInteraction, B_LevelEnd;

    public GameObject Go_Tool;
    public Vector2 V2_DummyTool;

   // public int[] I_

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);

        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        ResetDatas();
        BoardManager.Instance.InitialiseBlocs();

        Cursor.visible = false;
        //Go_Tool.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !B_DisableInteraction)
        {
            BoardManager.Instance.ResetTable();
            InterfaceManager.Instance.InitialiseInterface();
            ResetDatas();
        }
    }

    public void ResetDatas()
    {
        I_TresorCollected = 0;
        I_BlocDestroyed = 0;
        I_NumberTresor_Array.Initialize();
        I_NumberTresor_Array = new int[FossileData.Instance.Fossiles_List.Count];
        I_NumberOre_Array.Initialize();
        I_NumberOre_Array = new int[OresData.Instance.Ores_Ar.Length];

    }

    public void CollectTresor(int number, int type)
    {
        I_TresorCollected += number;
        if (type <= I_NumberTresor_Array.Length)
        {
            I_NumberTresor_Array[type]++;
        }
        else
        {
            Debug.LogError("Problem with array of numberTresor");
        }
        
    }

    public void CollectOre(int number, int type)
    {
        if (type <= I_NumberTresor_Array.Length)
        {
            I_NumberOre_Array[type] += number;
        }
        else
        {
            Debug.LogError("Problem with array of numberOres");
        }
    }

    public IEnumerator UsingTool(BlocBehaviour blocBehaviour)
    {
        B_DisableInteraction = true;//retire toute les intéractions avec le board

        ToolBehaviour ToolBehav = Go_Tool.GetComponent<ToolBehaviour>();// pour écrire plus simplement le toolBehaviour
        List<BlocBehaviour> blocs = BoardManager.Instance.Bloc_List;

        ToolBehav.UseTool();
        yield return new WaitForSeconds(0.25f);

        Camera.main.DOShakePosition(0.1f, 0.2f, 2 , 10);

        BoardManager board = BoardManager.Instance;
        switch (I_ToolType)
        {
            case 0:
                blocBehaviour.MinedOres();
                break;

            case 1:

                if (blocBehaviour.Index - board.I_BlocX > 0 && !blocs[blocBehaviour.Index - board.I_BlocX].B_IsMined)
                    blocs[blocBehaviour.Index - board.I_BlocX].MinedOres();//bloc du dessous

                if (blocBehaviour.Index + board.I_BlocX < blocs.Count && !blocs[blocBehaviour.Index + board.I_BlocX].B_IsMined)
                    blocs[blocBehaviour.Index + board.I_BlocX].MinedOres();//bloc du dessus

                goto case 0;

            case 2:
            case 3:

                if ((blocBehaviour.Index) % board.I_BlocX != 0 && !blocs[blocBehaviour.Index - 1].B_IsMined)
                    blocs[blocBehaviour.Index - 1].MinedOres();// gauche 

                if ((blocBehaviour.Index) % board.I_BlocX != board.I_BlocX - 1 && !blocs[blocBehaviour.Index + 1].B_IsMined)
                    blocs[blocBehaviour.Index + 1].MinedOres();//droite

                if (I_ToolType == 3)
                    goto case 1;
                goto case 0;
        }// change de manière de détruire en fonction de ToolType

        ToolBehav.InitTool();
        //I_ToolType = Random.Range(0, 4);// défini le type d'outil, ainsi il y a trois types possibles pour le moment

        foreach(TresorBehaviour tresors in board.Tresor_List)
        {
            tresors.CheckIfDiscovered();
        }
        foreach(OreBehaviour ores in board.Ore_List)//[OBSOLETE]
        {
            ores.CheckIfDiscovered();
        }

        B_DisableInteraction = false;

    }

    public void EndLevel()
    {
        Go_Tool.SetActive(false);
        B_LevelEnd = true;
        Cursor.visible = true;
        InterfaceManager.Instance.ShowEndPanel();
        int[] FinalNumbers = {I_TresorCollected,I_BlocDestroyed, I_OresCollected,50 };
        Debug.Log((recapPanel != null) + "  " + FinalNumbers.Length);
        recapPanel.DoDisplayNumbers(FinalNumbers);
        
    }


    public void CollectTresors()
    {
        GameManager GM = GameManager.Instance;

        for (int i= 0;i< GM.I_NumberTresor_Array.Length;i++)
        {
            GM.I_NumberTresor_Array[i] += I_NumberTresor_Array[i];
        }

        //for(int i = 0; i < GM.I_NumberOre_Array.Length; i++)
        //{
        //    GM.I_NumberOre_Array[i] += I_NumberOre_Array[i];
        //}
    }


    public void QuitLevel()
    {
        CollectTresors();
        GameManager.Instance.gameObject.GetComponent<GameSaveData>().SaveDatas();//saugarde
        SceneManager.LoadScene(0);
    }
}
