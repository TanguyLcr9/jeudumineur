﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelTresor : MonoBehaviour
{
    int ID;
    public Button Bu_Supprimer;
    public Image Im_Fossile;
    public TextMeshProUGUI TMp_Name;
    public TextMeshProUGUI TMp_Num;

    public void Initialise(int id)
    {
        FossileData Fdata = FossileData.Instance;
        ID = id;
        if (Fdata.Fossiles_List[id].Sprite != null)
        {
            Im_Fossile.sprite = Fdata.Fossiles_List[id].Sprite;
        }
        else
        {
            Debug.Log("No Image available for " + ID + " fossile");
        }

        if (Fdata.Fossiles_List[id].Name != "")
        {
            TMp_Name.text = "Fossile de " + Fdata.Fossiles_List[id].Name;
        }
        else
        {
            Debug.Log("No Name available for " + ID + " fossile");
        }

        TMp_Num.text = "x" + 0;
        
        gameObject.SetActive(false);
    }

    public void UpdateDatas()
    {
        
            gameObject.SetActive(LevelManager.Instance.I_NumberTresor_Array[ID] > 0?true:false);
        TMp_Num.text = "x" + LevelManager.Instance.I_NumberTresor_Array[ID];
        
    }

    public void SuppElement()
    {
        if (LevelManager.Instance.I_NumberTresor_Array[ID] > 0)
        {
            LevelManager.Instance.I_NumberTresor_Array[ID]--;
        }
        UpdateDatas();
    }

}
